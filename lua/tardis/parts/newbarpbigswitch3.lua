local PART={}
PART.ID = "newbarpbigswitch3"
PART.Name = "1977 TARDIS Big Switch 3"
PART.Model = "models/doctormemes/newbarp/bigswitch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctormemes/newbarp/big switch.wav"
PART.ShouldTakeDamage = true
PART.Control = "teleport"

if SERVER then
	function PART:Use(ply)
		if self.exterior:GetData("teleport") == true or self.exterior:GetData("vortex") == true
		or not self.interior:GetSequencesEnabled()
		then
		TARDIS:Control("teleport", ply)
		else
		TARDIS:ErrorMessage(ply, "Control Sequences are enabled. You must use the sequence.")
		end
	end
end

TARDIS:AddPart(PART,e)