local PART={}
PART.ID = "newbarparrow"
PART.Name = "1977 TARDIS Arrow"
PART.Model = "models/doctormemes/newbarp/arrow.mdl"
PART.AutoSetup = true
PART.Animate = true

if CLIENT then

	function PART:Initialize()
	self.arrow={}
	self.arrow.pos=0
	self.arrow.mode=1
	end

	function PART:Think()

		local exterior=self.exterior
		local interior=self.interior
		local power=self.exterior:GetData("power-state")

		if (self.arrow.pos>0 and not power == true) or power == true then
				if self.arrow.pos==0 then
					self.arrow.pos=1
				elseif self.arrow.pos==1 and power == true then
					self.arrow.pos=0
				end
				
			self.arrow.pos=math.Approach( self.arrow.pos, self.arrow.mode, FrameTime()*0.1 )
			self:SetPoseParameter( "motion", self.arrow.pos )
		end
	end
end

TARDIS:AddPart(PART)