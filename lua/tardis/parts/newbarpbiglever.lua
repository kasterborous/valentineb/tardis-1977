local PART={}
PART.ID = "newbarpbiglever"
PART.Name = "1977 TARDIS Big Lever"
PART.Model = "models/doctormemes/newbarp/biglever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.6
PART.Sound = "doctormemes/newbarp/biglever.wav"
PART.ShouldTakeDamage = true
PART.Control = "toggle_scanners"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control("toggle_scanners",ply)
		local power=self.exterior:GetData("power-state")
		if	power == nil then
		else
		self:EmitSound(Sound("doctormemes/newbarp/bs1.wav"))
		end
	end
end

TARDIS:AddPart(PART,e)