local PART={}
PART.ID = "newbarpbigdoors"
PART.Name = "1977 TARDIS Big Doors"
PART.Model = "models/doctormemes/newbarp/bigdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.35
PART.ShouldTakeDamage = true

if SERVER then
	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:EmitSound( Sound( "doctormemes/newbarp/big doors.wav" ))
		else
			self:SetOn( false )
			self:EmitSound( Sound( "doctormemes/newbarp/big doors.wav" ))
		end
	end
end

TARDIS:AddPart(PART,e)