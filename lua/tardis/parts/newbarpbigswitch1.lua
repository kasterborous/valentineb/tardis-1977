local PART={}
PART.ID = "newbarpbigswitch1"
PART.Name = "1977 TARDIS Big Switch 1"
PART.Model = "models/doctormemes/newbarp/bigswitch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctormemes/newbarp/big switch.wav"
PART.ShouldTakeDamage = true
PART.Control = "door"

TARDIS:AddPart(PART,e)