local PART={}
PART.ID = "newbarpbigredbutton"
PART.Name = "1977 TARDIS Big Red Button"
PART.Model = "models/doctormemes/newbarp/bigredbutton.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/newbarp/roundswitch.wav"
PART.ShouldTakeDamage = true
PART.Control = "physlock"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control("physlock",ply)
		local power=self.exterior:GetData("power-state")
		if	power == nil then
		else
		self:EmitSound(Sound("doctormemes/newbarp/bs1.wav"))
		end
	end
end

TARDIS:AddPart(PART,e)