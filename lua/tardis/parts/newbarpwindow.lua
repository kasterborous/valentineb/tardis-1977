local PART={}
PART.ID = "newbarpwindow"
PART.Name = "1977 TARDIS Scanner Window"
PART.Model = "models/doctormemes/newbarp/window.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.6

if SERVER then

	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:EmitSound( Sound( "doctormemes/newbarp/window.wav" ))
		else
			self:SetOn( false )
			self:EmitSound( Sound( "doctormemes/newbarp/window.wav" ))
		end
	end
end

TARDIS:AddPart(PART,e)