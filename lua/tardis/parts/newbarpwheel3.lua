local PART={}
PART.ID = "newbarpwheel3"
PART.Name = "1977 TARDIS Wheel 3"
PART.Model = "models/doctormemes/newbarp/wheel3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.5
PART.Sound = "doctormemes/newbarp/skrrrra.wav"
PART.ShouldTakeDamage = true
PART.Control = "float"

TARDIS:AddPart(PART,e)