local PART={}
PART.ID = "newbarproundel"
PART.Name = "1977 TARDIS Roundel"
PART.Model = "models/doctormemes/newbarp/roundel.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true
PART.Collision = true

if SERVER then

	function PART:Use(activator,ply)

		local room = TARDIS:GetPart(self.interior,"newbarproom")
		local door = TARDIS:GetPart(self.interior,"newbarpinteriordoor")
        local icube2 = TARDIS:GetPart(self.interior,"newbarpicube2")

		if ( self:GetOn() ) then
            self:EmitSound(Sound("doctormemes/newbarp/bs5.wav"))
            door:Toggle( !door:GetOn(), activator )
            icube2:SetCollide(true,true)
            room:SetRenderMode( RENDERMODE_TRANSALPHA )
            room:SetColor (Color(255,255,255,0))
            room:SetCollide(false,true)
		else
			self:EmitSound(Sound("doctormemes/newbarp/bs5.wav"))
            door:Toggle( !door:GetOn(), activator )
            icube2:SetCollide(false,true)
            room:SetRenderMode( RENDERMODE_NORMAL )
            room:SetColor (Color(255,255,255,255))
            room:SetCollide(true,true)
		end
 	end
end


TARDIS:AddPart(PART,e)