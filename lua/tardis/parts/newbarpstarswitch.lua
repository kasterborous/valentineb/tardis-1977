local PART={}
PART.ID = "newbarpstarswitch"
PART.Name = "1977 TARDIS Star Switch"
PART.Model = "models/doctormemes/newbarp/starswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/newbarp/roundswitch.wav"
PART.ShouldTakeDamage = true
PART.Control = "hads"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control("hads",ply)
		local power=self.exterior:GetData("power-state")
		if	power == nil then
		else
		self:EmitSound(Sound("doctormemes/newbarp/bs2.wav"))
		end
	end
end

TARDIS:AddPart(PART,e)