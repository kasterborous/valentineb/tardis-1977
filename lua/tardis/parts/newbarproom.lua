local PART={}
PART.ID = "newbarproom"
PART.Name = "1977 TARDIS Room"
PART.Model = "models/doctormemes/newbarp/room.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true
PART.Collision = true

if SERVER then

	function PART:Initialize()

		self:SetCollide(false,true)
		self:SetRenderMode( RENDERMODE_TRANSALPHA )
		self:SetColor (Color(255,255,255,0))
	end

end

TARDIS:AddPart(PART,e)