-- Adds 1977 TARDIS Rotor

local PART={}
PART.ID = "newbarprotor"
PART.Name = "1977 TARDIS Rotor"
PART.Model = "models/doctormemes/newbarp/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.ShouldTakeDamage = true

if CLIENT then

	function PART:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	end

	function PART:Think()

		local exterior=self.exterior
		local interior=self.interior
		local console=interior:GetPart("console")
		local rotor=interior:GetPart("newbarprotor")

		if (self.timerotor.pos>0 and not exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) or (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
				if self.timerotor.pos==0 then
					self.timerotor.pos=1
				elseif self.timerotor.pos==1 and (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
					self.timerotor.pos=0
				end
				
			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.3 )
			self:SetPoseParameter( "motion", self.timerotor.pos )
		end

		if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") or exterior:GetData("float") then
			rotor:SetSubMaterial(6 , "models/doctormemes/newbarp/Lamp On")
			rotor:SetSubMaterial(7 , "models/doctormemes/newbarp/rotor led (on)")
		else
			rotor:SetSubMaterial(6 , "models/doctormemes/newbarp/Lamp")
			rotor:SetSubMaterial(7 , "models/doctormemes/newbarp/rotor led")
		end
	end
end

TARDIS:AddPart(PART)