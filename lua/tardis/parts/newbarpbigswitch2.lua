local PART={}
PART.ID = "newbarpbigswitch2"
PART.Name = "1977 TARDIS Big Switch 2"
PART.Model = "models/doctormemes/newbarp/bigswitch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctormemes/newbarp/big switch.wav"
PART.ShouldTakeDamage = true
PART.Control = "door"

TARDIS:AddPart(PART,e)