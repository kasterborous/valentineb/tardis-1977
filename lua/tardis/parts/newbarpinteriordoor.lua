local PART={}
PART.ID = "newbarpinteriordoor"
PART.Name = "1977 TARDIS Interior Door"
PART.Model = "models/doctormemes/newbarp/interiordoor.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 1.3
PART.ShouldTakeDamage = true

if SERVER then
	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:EmitSound( Sound( "doctormemes/newbarp/interiordooropen.wav" ))
		else
			self:SetOn( false )
			self:EmitSound( Sound( "doctormemes/newbarp/interiordoorclose.wav" ))
		end
	end
end

TARDIS:AddPart(PART,e)