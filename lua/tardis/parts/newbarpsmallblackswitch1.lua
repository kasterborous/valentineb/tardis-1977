local PART={}
PART.ID = "newbarpsmallblackswitch1"
PART.Name = "1977 TARDIS Small Black Switch 1"
PART.Model = "models/doctormemes/newbarp/smallblackswitch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/newbarp/toggle.wav"
PART.ShouldTakeDamage = true

if SERVER then
	function PART:Use(activator)
		local ceiling=self.interior:GetPart("newbarpceiling")
		if ( self:GetOn() ) then
			self:EmitSound(Sound("doctormemes/newbarp/bs4.wav"))
			ceiling:SetRenderMode( RENDERMODE_NORMAL )
			ceiling:SetColor (Color(255,255,255,255))
		else
			self:EmitSound(Sound("doctormemes/newbarp/bs5.wav"))
			ceiling:SetRenderMode( RENDERMODE_TRANSALPHA )
			ceiling:SetColor (Color(255,255,255,0))
		end
	end
end

TARDIS:AddPart(PART,e)