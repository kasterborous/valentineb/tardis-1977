local PART={}
PART.ID = "newbarpolddoorsbigswitch1"
PART.Name = "1977 TARDIS Big Switch 1"
PART.Model = "models/doctormemes/newbarp/bigswitch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctormemes/newbarp/big switch.wav"
PART.ShouldTakeDamage = true

if SERVER then

	function PART:Use(activator,ply)
		local intdoors = TARDIS:GetPart(self.interior,"newbarpbigdoors")
		local icube = TARDIS:GetPart(self.interior,"newbarpicube")

		if ( self:GetOn() ) then
			intdoors:Toggle( !intdoors:GetOn(), activator )
			icube:SetCollide(false,true)
		else
			intdoors:Toggle( !intdoors:GetOn(), activator )
			icube:SetCollide(true,true)
		end
 	end
end

TARDIS:AddPart(PART,e)