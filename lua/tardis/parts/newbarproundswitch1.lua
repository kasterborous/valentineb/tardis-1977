local PART={}
PART.ID = "newbarproundswitch1"
PART.Name = "1977 TARDIS Round Switch 1"
PART.Model = "models/doctormemes/newbarp/roundswitch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/newbarp/roundswitch.wav"
PART.ShouldTakeDamage = true
PART.Control = "handbrake"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control("handbrake",ply)
		local power=self.exterior:GetData("power-state")
		if	power == nil then
		else
		self:EmitSound(Sound("doctormemes/newbarp/bs5.wav"))
		end
	end
end

TARDIS:AddPart(PART,e)