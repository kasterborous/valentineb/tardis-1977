local PART={}
PART.ID = "newbarpwhitedisks"
PART.Name = "1977 TARDIS White Disks"
PART.Model = "models/doctormemes/newbarp/whitedisks.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctormemes/newbarp/spin switch.wav"
PART.ShouldTakeDamage = true
PART.Control = "destination"

TARDIS:AddPart(PART,e)