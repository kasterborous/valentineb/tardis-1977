local PART={}
PART.ID = "newbarplittleswitch8"
PART.Name = "1977 TARDIS Little Switch 8"
PART.Model = "models/doctormemes/newbarp/littleswitch8.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/newbarp/little switch.wav"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)