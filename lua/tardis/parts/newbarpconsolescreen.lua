local PART={}
PART.ID = "newbarpconsolescreen"
PART.Name = "1977 TARDIS Console Screen"
PART.Model = "models/doctormemes/newbarp/consolescreen.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true
PART.BypassIsomorphic = true
PART.Control = "thirdperson"

if SERVER then    

	function PART:Think()
   	local exterior=self.exterior
	local physlock=self.exterior:GetData("physlock")
	local warning=self.exterior:GetData("health-warning")
	local fastremat=self.exterior:GetData("demat-fast")

		if exterior:GetData("vortex") and warning == true and physlock == true and fastremat == true then
			self:SetSubMaterial(0, "models/doctormemes/newbarp/blocks")
		elseif exterior:GetData("vortex") then
			self:SetSubMaterial(0 , "models/doctorwho1200/baker/1974vortex")
		else
			self:SetSubMaterial(0 , "models/doctormemes/newbarp/black")
		end
	end	
end


TARDIS:AddPart(PART,e)