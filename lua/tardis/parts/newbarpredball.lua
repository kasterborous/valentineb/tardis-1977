local PART={}
PART.ID = "newbarpredball"
PART.Name = "1977 TARDIS Red Ball"
PART.Model = "models/doctormemes/newbarp/redball.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/newbarp/roundswitch.wav"
PART.ShouldTakeDamage = true
PART.Control = "isomorphic"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control("isomorphic",ply)
		local power=self.exterior:GetData("power-state")
		if	power == nil then
		else
		self:EmitSound(Sound("doctormemes/newbarp/bs3.wav"))
		end
	end
end

TARDIS:AddPart(PART,e)