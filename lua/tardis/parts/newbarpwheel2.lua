local PART={}
PART.ID = "newbarpwheel2"
PART.Name = "1977 TARDIS Wheel 2"
PART.Model = "models/doctormemes/newbarp/wheel2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.5
PART.Sound = "doctormemes/newbarp/skrrrra.wav"
PART.ShouldTakeDamage = true
PART.Control = "flight"

TARDIS:AddPart(PART,e)