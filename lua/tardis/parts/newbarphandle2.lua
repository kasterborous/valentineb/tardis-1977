local PART={}
PART.ID = "newbarphandle2"
PART.Name = "1977 TARDIS Handle 2"
PART.Model = "models/doctormemes/newbarp/handle2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4.5
PART.Sound = "doctormemes/newbarp/handle.wav"
PART.ShouldTakeDamage = true
PART.Control = "engine_release"

TARDIS:AddPart(PART,e)