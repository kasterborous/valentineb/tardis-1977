local PART={}
PART.ID = "newbarpsmallblackswitch3"
PART.Name = "1977 TARDIS Small Black Switch 3"
PART.Model = "models/doctormemes/newbarp/smallblackswitch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/newbarp/toggle.wav"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)