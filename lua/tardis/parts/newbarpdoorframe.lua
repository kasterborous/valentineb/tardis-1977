local PART={}
PART.ID = "newbarpdoorframe"
PART.Name = "1977 TARDIS Door Frame"
PART.Model = "models/doctorwho1200/newbarp/doorframe.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = true

if SERVER then
	hook.Add("SkinChanged", "newbarp-doorframe", function(ent,i)
		if ent.TardisExterior then
			local newbarpdoorframe=ent:GetPart("newbarpdoorframe")
			if IsValid(newbarpdoorframe) then
				newbarpdoorframe:SetSkin(i)
			end
			if IsValid(ent.interior) then
				local newbarpdoorframe=ent.interior:GetPart("newbarpdoorframe")
				if IsValid(newbarpdoorframe) then
					newbarpdoorframe:SetSkin(i)
				end
			end
		end
	end)
end

TARDIS:AddPart(PART)