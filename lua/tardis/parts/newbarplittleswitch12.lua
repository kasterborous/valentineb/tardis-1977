local PART={}
PART.ID = "newbarplittleswitch12"
PART.Name = "1977 TARDIS Little Switch 12"
PART.Model = "models/doctormemes/newbarp/littleswitch12.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/newbarp/little switch.wav"
PART.ShouldTakeDamage = true
PART.Control = "redecorate"

TARDIS:AddPart(PART,e)