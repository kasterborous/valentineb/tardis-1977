local PART={}
PART.ID = "newbarpblackvalve"
PART.Name = "1977 TARDIS Black Valve"
PART.Model = "models/doctormemes/newbarp/blackvalve.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctormemes/newbarp/roundswitch.wav"
PART.ShouldTakeDamage = true
PART.Control = "repair"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control("repair",ply)
		local power=self.exterior:GetData("power-state")
		if	power == nil then
		else
		self:EmitSound(Sound("doctormemes/newbarp/bs1.wav"))
		end
	end
end

TARDIS:AddPart(PART,e)