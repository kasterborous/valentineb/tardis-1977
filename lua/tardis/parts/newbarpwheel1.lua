local PART={}
PART.ID = "newbarpwheel1"
PART.Name = "1977 TARDIS Wheel 1"
PART.Model = "models/doctormemes/newbarp/wheel1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.5
PART.Sound = "doctormemes/newbarp/skrrrra.wav"
PART.ShouldTakeDamage = true
PART.Control = "power"

if SERVER then
	
	function PART:Think(ply)
	local power=self.exterior:GetData("power-state")
	local interior=self.interior
	local console=interior:GetPart("newbarpconsole")
	local coloured=interior:GetPart("newbarp4coloured")
	local warning=self.exterior:GetData("health-warning")
	local roundel=interior:GetPart("newbarproundel")

		if	power == false then
				interior:SetSubMaterial(5 , "models/doctormemes/newbarp/roundel unlit")
				interior:SetSubMaterial(15 , "models/doctormemes/newbarp/studio unlit")
				console:SetSubMaterial(13 , "models/doctormemes/newbarp/little lamps off")
				console:SetSubMaterial(11 , "models/doctormemes/newbarp/square lights off")
				coloured:SetSubMaterial(0 , "models/doctormemes/newbarp/4coloured lights off")
				roundel:SetSubMaterial(0 , "models/doctormemes/newbarp/roundel unlit")
		else

			if warning == true then
				interior:SetSubMaterial(15 , "models/doctormemes/newbarp/studio warn")
			else
				interior:SetSubMaterial(15 , "models/doctormemes/newbarp/studio light")
			end

				interior:SetSubMaterial(5 , "models/doctormemes/newbarp/roundel")
				console:SetSubMaterial(13 , "models/doctormemes/newbarp/little lamps")
				console:SetSubMaterial(11 , "models/doctormemes/newbarp/square lights")
				coloured:SetSubMaterial(0 , "models/doctormemes/newbarp/4coloured lights")
				roundel:SetSubMaterial(0 , "models/doctormemes/newbarp/roundel")
		end
	end
end

TARDIS:AddPart(PART,e)