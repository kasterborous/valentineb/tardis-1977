local PART={}
PART.ID = "newbarpicube2"
PART.Name = "1977 TARDIS icube2"
PART.Model = "models/doctormemes/newbarp/icube2.mdl"
PART.AutoSetup = true

if SERVER then
	
	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:Initialize()
		self:Collide( true )
	end
	
end

TARDIS:AddPart(PART,e)