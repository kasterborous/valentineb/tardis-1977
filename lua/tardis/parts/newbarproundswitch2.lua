local PART={}
PART.ID = "newbarproundswitch2"
PART.Name = "1977 TARDIS Round Switch 2"
PART.Model = "models/doctormemes/newbarp/roundswitch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctormemes/newbarp/roundswitch.wav"
PART.ShouldTakeDamage = true

if SERVER then

	function PART:Use(activator,ply)
		local window = TARDIS:GetPart(self.interior,"newbarpwindow")

		if ( self:GetOn() ) then
			window:Toggle( !window:GetOn(), activator )
		else
			window:Toggle( !window:GetOn(), activator )
		end
 	end
end

TARDIS:AddPart(PART,e)