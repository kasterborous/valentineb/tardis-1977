local PART={}
PART.ID = "newbarphandle3"
PART.Name = "1977 TARDIS Handle 3"
PART.Model = "models/doctormemes/newbarp/handle3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4.5
PART.Sound = "doctormemes/newbarp/handle.wav"
PART.ShouldTakeDamage = true
PART.Control = "fastreturn"

TARDIS:AddPart(PART,e)