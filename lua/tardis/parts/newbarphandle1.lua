local PART={}
PART.ID = "newbarphandle1"
PART.Name = "1977 TARDIS Handle 1"
PART.Model = "models/doctormemes/newbarp/handle1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4.5
PART.Sound = "doctormemes/newbarp/handle.wav"
PART.ShouldTakeDamage = true
PART.Control = "doorlock"

TARDIS:AddPart(PART,e)