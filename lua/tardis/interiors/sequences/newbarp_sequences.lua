--1977 TARDIS Interior - Control Sequences (advanced mode)

local Seq = {
    ID = "newbarp_sequences",

    ["newbarpredbuttons1"] = {
        Controls = {
            "newbarpwheel2",
            "newbarpwhitebuttons",
            "newbarpstarswitch",
	    	"newbarpbiglever",
	   		"newbarproundswitch1",
	   		"newbarpbigswitch3"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    },
	["newbarpredbuttons2"] = {
        Controls = {
            "newbarpwheel2",
            "newbarpwhitebuttons",
            "newbarpstarswitch",
	    	"newbarpbiglever",
	    	"newbarproundswitch1",
	    	"newbarpbigswitch3"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    },
	["newbarpwhitedisks"] = {
        Controls = {
            "newbarpwheel2",
            "newbarpwhitebuttons",
            "newbarpstarswitch",
	    	"newbarpbiglever",
	    	"newbarproundswitch1",
	    	"newbarpbigswitch3"
        },
        OnFinish = function(self)
		if self.exterior:GetData("vortex") then
            		if IsValid(self) and IsValid(self) then
               			self.exterior:Mat()
            		end
      		end
		if not self.exterior:GetData("vortex") then
		        if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end
	end
    }
}

TARDIS:AddControlSequence(Seq)