-- 1977 TARDIS (CLASSIC DOORS WOW OMG SO GOOD)

local T={}
T.Base="base"
T.Name="1977 TARDIS"
T.ID="newbarp"
T.EnableClassicDoors = true
T.Versions = {
    main = {
        classic_doors_id = "newbarp",
        double_doors_id = "newbarpolddoors",
    },
}
T.Interior={
	Model="models/doctormemes/newbarp/interior.mdl",
	Portal={
		pos=Vector(0,-132,47.85),
		ang=Angle(0,90,0),
		width=69,
		height=93
	},
	Fallback={
		pos=Vector(0,-106.5,3),
		ang=Angle(0,90,0),
	},
	Scanners = {
		{
			mat = "models/doctormemes/newbarp/scanner",
			width = 1024,
			height = 1024,
			ang = Angle(0,0,0),
			fov = 60,
		}
	},  
--	Screens={
--		{
--       	   pos=Vector(-67.3175,105.07,98.3749),
--        	   ang=Angle(0,30.8,90),
--        	   width=960,
--        	   height=800,
--		   visgui_rows=6
--		}
--	},
	ScreensEnabled= false,
	Sounds={
		Power={
			On="doctormemes/newbarp/bspowerup.wav",
			Off="doctormemes/newbarp/bspowerdown.wav"
		},
		Door={
			enabled=true,
			open = "doctormemes/newbarp/big doors.wav",
			close = "doctormemes/newbarp/big doors.wav",
		},
	},
	ExitDistance=360, -- noscope
	Sequences="newbarp_sequences",
	Parts={
		intdoor = { model = "models/doctormemes/newbarp/bigdoorsclassic.mdl", },
		newbarprotor=true,
		newbarpdoorframe=true,
		newbarpwindow=true,
		newbarp4coloured=true,
		newbarpbigswitch3=true,
		newbarpbigswitch1=true,
		newbarpbigswitch2=true,
		newbarppanelsbox=true,
		newbarpcurvedbuttons=true,
		newbarproundswitch2=true,
		newbarpbigblackvent=true,
		newbarproundswitch1=true,
		newbarpcubeybuttons=true,
		newbarproundscreen=true,
		newbarpredbuttons1=true,
		newbarpredbuttons2=true,
		newbarpwheel1=true,
		newbarpwheel2=true,
		newbarpwheel3=true,
		newbarpwhitedisks=true,
		newbarpceiling=true,
		newbarpconsole=true,
		newbarpconsolescreen=true,
		newbarpbiglever=true,
		newbarpblackvalve=true,
		newbarpbeveledlevers=true,
		newbarpwhitebuttons=true,
		newbarpstarswitch=true,
		newbarpredball=true,
		newbarpblackball=true,
		newbarpbigredbutton=true,
		newbarphandle3=true,
		newbarphandle2=true,
		newbarphandle1=true,
		newbarpsmallblackswitch1=true,
		newbarpsmallblackswitch2=true,
		newbarpsmallblackswitch3=true,
		newbarplittleswitch1=true,
		newbarplittleswitch2=true,
		newbarplittleswitch3=true,
		newbarplittleswitch4=true,
		newbarplittleswitch5=true,
		newbarplittleswitch6=true,
		newbarplittleswitch7=true,
		newbarplittleswitch8=true,
		newbarplittleswitch9=true,
		newbarplittleswitch10=true,
		newbarplittleswitch11=true,
		newbarpglass=true,
		newbarparrow=true,
		newbarplittleswitch12=true,
		newbarpinteriordoor=true,
		newbarproom=true,
		newbarpicube2=true,
		newbarproundel=true,
	
		door={
			model="models/doctorwho1200/newbarp/intdoor.mdl",posoffset=Vector(3.37,0,-45.68),angoffset=Angle(0,-180,0)
		},
	},
	IdleSound={
		{
			path="doctormemes/newbarp/interior.wav",
			volume=1	
		}
	},
	LightOverride={
		basebrightness=0.3,
		nopowerbrightness=0.2
	},
	Light={
		color=Color(255,255,255),
		warncolor=Color(255,0,0),
		pos=Vector(0,0,115),
		brightness=0.3
	},
	TipSettings={
			style="classic",
            		view_range_max=60,
            		view_range_min=50,
	},
	PartTips={
		newbarpbigswitch3	=	{pos=Vector(34.72, 19.986, 40.838), text="Demat Switch", down = true},
		newbarpredbuttons1	=	{pos=Vector(16.479, -18.169, 45.206), text="Coordinates"},
		newbarpwheel2	=	{pos=Vector(32.782, -6.058, 43.506), text="Flight", right = true},
		newbarpbiglever	=	{pos=Vector(-12.027, 36.88, 41.07), text="Scanner", right = true},
		newbarproundswitch1	=	{pos=Vector(28.128, 26.32, 40.995), text="Handbrake", right = true, down = true},
		newbarpwhitedisks	=	{pos=Vector(20.819, -12.127, 45.369), text="Manual Destination Selection", right = true},
		newbarpwheel3	=	{pos=Vector(21.85, -25.363, 43.245), text="Float"},
		newbarproundswitch2	=	{pos=Vector(27.324, -15.157, 43.228), text="Scanner Window", right = true, down = true},
		newbarpbigswitch1	=	{pos=Vector(-1.767, -39.413, 41.367), text="Door Switch", down = true},
		newbarpbigswitch2	=	{pos=Vector(1.767, -39.413, 41.367), text="Door Switch", right = true, down = true},
		newbarpwheel1	=	{pos=Vector(-22.286, -25.008, 43.446), text="Power", right = true},
		newbarpblackvalve	=	{pos=Vector(-25.42, -34.994, 40.206), text="Repair", right = true, down = true},
		newbarpwhitebuttons	=	{pos=Vector(-28.757, -25.906, 40.608), text="Vortex Flight"},
		newbarpstarswitch	=	{pos=Vector(-18.233, 20.894, 45.78), text="H.A.D.S."},
		newbarpredball	=	{pos=Vector(-28.458, 5.542, 45.384), text="Security", right = true},
		newbarpblackball	=	{pos=Vector(26.187, -34.541, 40.835), text="Cloaking Device", down = true},
		newbarpbigredbutton	=	{pos=Vector(42.891, -5.52, 40.773), text="Physical Lock", down = true, right = true},
		newbarphandle2	=	{pos=Vector(12.869, -38.549, 40.652), text="Engine Release", down = true, right = true},
		newbarphandle3	=	{pos=Vector(-32.901, -21.671, 40.146), text="Fast Return", down = true},
		newbarphandle1	=	{pos=Vector(-11.905, -38.198, 41.021), text="Door Lock", down = true},
		newbarplittleswitch12	=	{pos=Vector(-39.075, -4.128, 41.786), text="Redecoration"},
	},
	CustomTips={
		{pos=Vector(-0.184, 27.111, 43.846), text="Third Person View", right = true},
	},
	IntDoorAnimationTime = 3,
}

T.Exterior={
	Model="models/doctorwho1200/newbarp/exterior.mdl",
	Mass=5000,
	DoorAnimationTime=0.6,
	ScannerOffset=Vector(30,0,50),	
	Portal={
		pos=Vector(28,0,45.7),
		ang=Angle(0,0,0),
		width=39,
		height=85
	},
	Fallback={
		pos=Vector(50,0,5),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=true,
		pos=Vector(0,0,108),
		color=Color(255,240,200)
	},
	Sounds={
		Lock="doctormemes/newbarp/lock.wav",
		Door={
			enabled=true,
			open="doctormemes/newbarp/doorext_open.wav",
			close="doctormemes/newbarp/doorext_close.wav"
		},
	},
	Parts={
		door={
			model="models/doctorwho1200/newbarp/extdoor.mdl",posoffset=Vector(-3.37,0,-45.68),angoffset=Angle(0,0,0),AnimateSpeed = 7
		},
		vortex={
			model="models/doctorwho1200/baker/1974timevortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,180,0),
			scale=10			
		}
	},
	Teleport={
		SequenceSpeed=0.77,
		SequenceSpeedFast=0.935,
		DematSequence={
			175,
			230,
			100,
			150,
			50,
			100,
			0
		},
		MatSequence={
			100,
			50,
			150,
			100,
			200,
			150,
			255
		}
	}
}

TARDIS:AddInterior(T)