-- 1977 TARDIS (Old doors because apparently you are a crybaby)

local T={}
T.Base="newbarp"
T.IsVersionOf = "newbarp"
T.Name="1977 TARDIS (Doors)"
T.ID="newbarpolddoors"
T.EnableClassicDoors = false
T.Interior={
	Model="models/doctormemes/newbarp/interior.mdl",
	Portal={
		pos=Vector(0,-226.88,43.38),
		ang=Angle(0,90,0),
		width=100,
		height=110
	},
	Fallback={
		pos=Vector(0,-200,3),
		ang=Angle(0,90,0),
	},
	Sounds={
		Door=false,
	},
	Parts={
		newbarpicube=true,
		newbarpbigdoors=true,
		newbarpolddoorsbigswitch1=true,
		newbarpbigswitch1=false,
		intdoor=false,
	
		door={
			model="models/doctorwho1200/newbarp/intdoor.mdl",posoffset=Vector(6.07,0,-44.98),angoffset=Angle(0,-180,0)
		},
	},
	PartTips={
		newbarpolddoorsbigswitch1	=	{pos=Vector(-1.767, -39.413, 41.367), text="Big Doors", down = true},
		newbarpbigswitch1 = false,
	},
}

T.Exterior={
	Portal={
		pos=Vector(30.7,0,45),
		ang=Angle(0,0,0),
		width=39,
		height=85
	},
	Parts={
		door={
			model="models/doctorwho1200/newbarp/extdoor.mdl",posoffset=Vector(-6.07,0,-44.98),angoffset=Angle(0,0,0),AnimateSpeed = 7
		},
	},
}

TARDIS:AddInterior(T)